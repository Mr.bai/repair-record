import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';

class TopBar extends StatefulWidget {
  const TopBar({super.key});

  @override
  State<TopBar> createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {

    return SizedBox(
        height: 31,
        child: Container(
          color: Colors.indigo,
            child: Column(
              children: [
                Row(
                  children: [
                    LeftSide(),
                    Expanded(
                      child: RightSide(),
                    )
                  ],
                ),
                // WindowTitleBarBox(child: MoveWindow()),
                //Expanded(child: Container())
              ],
            )));
  }
}

const sidebarColor = Color(0xffc710b3);
class LeftSide extends StatelessWidget {
  const LeftSide({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 200,
        child: Container(
            // color: sidebarColor,
            child: WindowTitleBarBox(child:
              MoveWindow(
                child: Container(
                  margin: EdgeInsets.only(left: 25),
                  child: Row(
                    children: [
                      Image.asset("assets/images/favicon.png", height: 20, width: 20,),
                      Text("维修检测功能", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                    ],
                  ),
                )
              ),
            ),
        )
    );
  }
}





const backgroundStartColor = Color(0xFFFFD500);
const backgroundEndColor = Color(0xFFF6A00C);

class RightSide extends StatelessWidget {
  const RightSide({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        // color: sidebarColor,
        // decoration: const BoxDecoration(
        //   gradient: LinearGradient(
        //       begin: Alignment.topCenter,
        //       end: Alignment.bottomCenter,
        //       colors: [backgroundStartColor, backgroundEndColor],
        //       stops: [0.0, 1.0]),
        // ),
        child: Column(children: [
          WindowTitleBarBox(
            child: Row(
              children: [Expanded(child: MoveWindow()), const WindowButtons()],
            ),
          )
        ]),
      );
  }
}

final buttonColors = WindowButtonColors(
    //iconNormal: const Color(0xFF805306),
    iconNormal: Colors.white,
    mouseOver: const Color(0xFFF6A00C),
    mouseDown: const Color(0xFF805306),
    iconMouseOver: const Color(0xFF805306),
    iconMouseDown: const Color(0xFFFFD500));

final closeButtonColors = WindowButtonColors(
    mouseOver: const Color(0xFFD32F2F),
    mouseDown: const Color(0xFFB71C1C),
    //iconNormal: const Color(0xFF805306),
    iconNormal: Colors.white,
    iconMouseOver: Colors.white);

class WindowButtons extends StatefulWidget {
  const WindowButtons({Key? key}) : super(key: key);

  @override
  _WindowButtonsState createState() => _WindowButtonsState();
}

class _WindowButtonsState extends State<WindowButtons> {
  void maximizeOrRestore() {
    setState(() {
      // appWindow.restore();
      appWindow.maximizeOrRestore();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [

        MinimizeWindowButton(colors: buttonColors),
        appWindow.isMaximized
            ? RestoreWindowButton(
          colors: buttonColors,
          onPressed: maximizeOrRestore,
        )
            : MaximizeWindowButton(
          colors: buttonColors,
          onPressed: maximizeOrRestore,
        ),
        CloseWindowButton(colors: closeButtonColors),

        // MinimizeWindowButton(colors: buttonColors),
        // appWindow.isMaximized
        //     ? RestoreWindowButton(
        //   colors: buttonColors,
        //   onPressed: maximizeOrRestore,
        // )
        //     : MaximizeWindowButton(
        //   colors: buttonColors,
        //   onPressed: maximizeOrRestore,
        // ),
        // CloseWindowButton(colors: buttonColors),
      ],
    );
  }
}
