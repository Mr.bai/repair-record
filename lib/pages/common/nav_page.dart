import 'package:flutter/material.dart';

class NavPage extends StatefulWidget {
  final ValueChanged<int>? onDestinationSelected;

  const NavPage({super.key, this.onDestinationSelected});

  @override
  State<NavPage> createState() => _NavPageState();
}

class _NavPageState extends State<NavPage> {

  bool _extended = false;
  int _selectedIndex = 0;
  final List<NavigationRailDestination> _destinations = const [
    NavigationRailDestination(icon: Icon(Icons.games), label: Text("设备检修")),
    NavigationRailDestination(icon: Icon(Icons.library_books), label: Text("检修记录")),
    NavigationRailDestination(icon: Icon(Icons.settings), label: Text("其他设置")),
  ];

  Widget buildLeading() {
    return GestureDetector(
      onTap: _toggleExtended,
      child: Icon(
        Icons.menu_open,
        color: Colors.indigoAccent,
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return NavigationRail(
      extended: _extended,
      leading: buildLeading(),
      destinations: _destinations,
      selectedIndex: _selectedIndex,
      onDestinationSelected: _onDestinationSelected,
      elevation: 1,
      trailing: Expanded(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.only(bottom: 20.0),
            child: Image.asset("assets/images/favicon.png", height: 35, width: 35,),
          ),
        ),
      ),



    );
  }

  void _onDestinationSelected(int value) {
    setState(() {
      _selectedIndex = value;
    });
    widget.onDestinationSelected?.call(value);
  }

  void _toggleExtended() {
    setState(() {
      _extended = !_extended;
    });
  }

}
