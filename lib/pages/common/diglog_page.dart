import 'package:flutter/material.dart';

class DialogTools {
 
  static void showAlertDialog(BuildContext context, String title, String content) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: [
            TextButton(child: Text("确认"), onPressed: () {
              Navigator.of(context).pop(false);
            }),
          ],
        );
      }
    );
  }
  
}