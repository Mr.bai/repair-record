import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/home/home_page.dart';
import 'package:maintenancerecord/pages/common/topbar_page.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({super.key});

  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: const [
          // 头部 窗口控制区域
          TopBar(),
          // 页面内容区域
          Expanded(child: HomePage())
        ],
      ),
    );
  }
}
