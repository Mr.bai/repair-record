import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/home/home_index_page.dart';
import 'package:maintenancerecord/pages/repair/repair_form.dart';
import 'package:maintenancerecord/pages/repair/repair_search.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // height: 200,
      // width: 300,
      child: Navigator(
        onPopPage: _onPopPage,
        initialRoute: "/home-index",
        onGenerateRoute: _onGenerateRoute,
        observers: [TolyNavigatorObservers()],
      ),
    );
  }


  bool _onPopPage(Route<dynamic> route, result) {
    print('-------_onPopPage------');
    return true;
  }

  Route _onGenerateRoute (RouteSettings settings) {
    switch (settings.name) {
      case '/home/index':
        return MaterialPageRoute(builder: (_) => const HomeIndexPage(), settings: settings);
      case "/repair/search":
        return MaterialPageRoute(builder: (_) => const RepairSearch(), settings: settings);
      case "/repair/form":
        return MaterialPageRoute(builder: (_) => const RepairForm(), settings: settings);
      // case "/green":
      //   return MaterialPageRoute(builder: (_) => const GreenPage(), settings: settings);
      default:
        return MaterialPageRoute(builder: (_) => const HomeIndexPage(), settings: settings);
    }
  }




}


//路由监听器
class TolyNavigatorObservers extends NavigatorObserver {
  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    // print(
    //     '--didPush:--route:--${route.settings}--previousRoute:--${previousRoute?.settings}');
  }

  @override
  void didStopUserGesture() {
    print('--didStopUserGesture:--');
  }

  @override
  void didStartUserGesture(Route<dynamic> route, Route<dynamic>? previousRoute) {
    print(
        '--didStartUserGesture:--route:--${route.settings}--previousRoute:--${previousRoute?.settings}');
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    print(
        '--didReplace:--newRoute:--${newRoute?.settings}--oldRoute:--${oldRoute?.settings}');
  }

  @override
  void didRemove(Route<dynamic>? route, Route<dynamic>? previousRoute) {
    print(
        '--didRemove:--route:--${route?.settings}--previousRoute:--${previousRoute?.settings}');
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    print(
        '--didPop:--route:--${route.settings}--previousRoute:--${previousRoute?.settings}');
  }
}
