import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/common/nav_page.dart';
import 'package:maintenancerecord/pages/records/record_index_page.dart';
import 'package:maintenancerecord/pages/repair/repair_search.dart';
import 'package:maintenancerecord/pages/setting/setting_index_page.dart';
import 'package:maintenancerecord/pages/setting/setting_page.dart';
import 'package:maintenancerecord/pages/setting/setting_test_page.dart';

class HomeIndexPage extends StatefulWidget {
  const HomeIndexPage({super.key});

  @override
  State<HomeIndexPage> createState() => _HomeIndexPageState();
}

class _HomeIndexPageState extends State<HomeIndexPage> {

  final PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          NavPage(
            onDestinationSelected: _onDestinationSelected,
          ),
          Expanded(
              child: PageView(
                controller: _controller,
                children: const [
                  RepairSearch(),
                  RecordIndexPage(content: '消息'),
                  // SettingIndexPage(content: '设置页面'),
                  // SettingPage(),
                  ParentWidget(),
                ],
              )
          )
        ],
      ),
    );
  }

  void _onDestinationSelected (int value) {
    _controller.jumpToPage(value);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

}
