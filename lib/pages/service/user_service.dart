import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:maintenancerecord/pages/common/diglog_page.dart';


class UserService {

  static void getUserInfo(BuildContext context, String cardNo, String empNo, Function func) async {
    Uri url = Uri.parse("http://172.25.2.242:8087/RepairRecord/search");
    http.post(url, body: {'cardNo': cardNo, 'empNo': empNo}).then((res) {

        if(res.statusCode == 200) {
          var decodedRes = jsonDecode(utf8.decode(res.bodyBytes)) as Map;
          func.call(decodedRes);
        } else {
          DialogTools.showAlertDialog(context, "提示", "系统异常：500");
          // return jsonDecode('{"code": 500}');
        }


    });
    // {
    //   if(res.statusCode == 200) {
    //     var decodedRes = jsonDecode(utf8.decode(res.bodyBytes)) as Map;
    //     return decodedRes;
    //   } else {
    //     DialogTools.showAlertDialog(context, "提示", "系统异常：500");
    //     // return jsonDecode('{"code": 500}');
    //   }
    //
    //
    //
    // }

    // return jsonDecode('{"code": 500}');
  }


}