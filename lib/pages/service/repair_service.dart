import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:maintenancerecord/pages/common/diglog_page.dart';

class RepairService {

  // String empNo, String optUser, Integer optType, Integer optState, String optRemark
  static void saveRecord (BuildContext context, String empNo, String optUser, String optType, String optState, String optRemark, Function func) {
    Uri url = Uri.parse("http://172.25.2.242:8087/RepairRecord/save");
    http.post(url, body: {'empNo': empNo, 'optUser': optUser, 'optType': optType, 'optState': optState, 'optRemark': optRemark}).then((res) {
      if(res.statusCode == 200) {
        var decodedRes = jsonDecode(utf8.decode(res.bodyBytes)) as Map;
        func.call(decodedRes);
      } else {
        DialogTools.showAlertDialog(context, "提示", "系统异常：500");
        // return jsonDecode('{"code": 500}');
      }


    });
  }

  static Future<dynamic> page(BuildContext context, String page, String userName, String date) async {
    Uri url = Uri.parse("http://172.25.2.242:8087/RepairRecord/page");
    Response response = await http.post(url, body: {'page': page, 'name': userName, 'date': date});
    // print(response.statusCode);
    return response;
    // var decodedRes = null;
    // print(response.statusCode);
    // if(response.statusCode == 200) {
    //   print(99);
    //   decodedRes = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
    //   return decodedRes;
    // } else {
    //   print(99);
    //   DialogTools.showAlertDialog(context, "提示", "系统异常：500");
    // // return jsonDecode('{"code": 500}');
    // }

    // response.then((res)  {
    //   // print(98),
    //   // print(res.statusCode),
    //   return res;
    // });

    // response.then((res) => {
    //   if(res.statusCode == 200) {
    //     decodedRes = jsonDecode(utf8.decode(res.bodyBytes)) as Map,
    //   } else {
    //     DialogTools.showAlertDialog(context, "提示", "系统异常：500"),
    //   // return jsonDecode('{"code": 500}');
    //   }
    // });


    // print(response);
    // return response;
    // http.post(url, body: {'page': page, 'name': userName, 'date': date}).then((res) {
    //   print(3);
    //   if(res.statusCode == 200) {
    //     var decodedRes = jsonDecode(utf8.decode(res.bodyBytes)) as Map;
    //     return decodedRes;
    //   } else {
    //     DialogTools.showAlertDialog(context, "提示", "系统异常：500");
    //     // return jsonDecode('{"code": 500}');
    //   }
    // });



  }

  static Future<dynamic> saveComment(BuildContext context, String recordId, String commentUser, String commentState,String commentDesc) async {
    Uri url = Uri.parse("http://172.25.2.242:8087/RepairRecord/comment/save");
    Response response = await http.post(url, body: { 'recordId': recordId, 'commentUser': commentUser, 'commentState': commentState, 'commentDesc': commentDesc });
    return response;
  }

  static Future<dynamic> getComments(BuildContext context, String recordId) async {
    Uri url = Uri.parse("http://172.25.2.242:8087/RepairRecord/comment/list");
    Response response = await http.post(url, body: { 'recordId': recordId });
    return response;
  }

}