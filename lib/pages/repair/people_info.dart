import 'package:flutter/material.dart';

class PeopleInfoPage extends StatefulWidget {

  final double winHeight;

  final Map? userInfo;

  const PeopleInfoPage({super.key, required this.winHeight, this.userInfo});

  @override
  State<PeopleInfoPage> createState() => _PeopleInfoPageState();
}

class _PeopleInfoPageState extends State<PeopleInfoPage> {

  TextStyle _propleTitle = TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.black87);
  TextStyle _propleInfo = TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black87);

  @override
  Widget build(BuildContext context) {
    //print(widget.userInfo);
    return Container(
      width: 250,
      height: widget.winHeight,
      // color: Colors.amberAccent,
      child: Center(
          child: Wrap(
            runSpacing: 5,
            children: [
              // 标题
              Container(
                margin: EdgeInsets.all(5),
                width: 240,
                child: Container(
                  alignment: Alignment.center,
                  // color: Colors.red,
                  height: 60,
                  width: 220,
                  child: Text("人员档案", style: _propleTitle,),
                ),
              ),

              // 照片
              Container(
                margin: EdgeInsets.all(5),
                width: 240,
                child: Container(
                  alignment: Alignment.center,
                  // color: Colors.grey,

                  child: SizedBox(
                    height: 200,
                    width: 140,
                    child: Container(
                      // color: Colors.indigoAccent,
                      //child:  ? Image.network(widget.userInfo?["img"], width: 130, height: 180,) : Image.asset("assets/images/people_pic.jpeg"),
                      child: ((widget.userInfo?["img"]).toString().isEmpty) ? Image.asset("assets/images/people_pic.jpeg") : Image.network(widget.userInfo?["img"],),
                    ),
                  ),
                ),
              ),

              // 人员资料
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.all(5),
                child: SizedBox(
                  width: 140,
                  child: Container(
                    // color: Colors.red,
                      child:Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: widget.userInfo?["sourceType"] == 'A' ? true : false,
                            child: Text('工号：${widget.userInfo?["empNo"]}', style: _propleInfo,),
                          ),
                          Visibility(
                            visible: widget.userInfo?["sourceType"] != 'A' ? true : false,
                            child: Text('学号：${widget.userInfo?["empNo"]}', style: _propleInfo,),
                          ),

                          Text("卡号：${widget.userInfo?["cardNo"]}", style: _propleInfo,),
                          Text("姓名：${widget.userInfo?["userName"]}", style: _propleInfo,),
                          Text("性别：${widget.userInfo?["gender"]}", style: _propleInfo,),
                          Visibility(
                              visible: widget.userInfo?["sourceType"] == 'A' ? true : false,
                              child: Text("职位：${widget.userInfo?["titleName"]}", style: _propleInfo,),
                          ),
                          Visibility(
                            visible: widget.userInfo?["sourceType"] == 'A' ? true : false,
                            child: Text("部门：${widget.userInfo?["deptName"]}", style: _propleInfo,),
                          ),
                          Visibility(
                              visible: widget.userInfo?["sourceType"] != 'A' ? true : false,
                              child: Text("年级：${widget.userInfo?["gradelevel"]}", style: _propleInfo,),
                          ),

                          Visibility(
                            visible: widget.userInfo?["sourceType"] != 'A' ? true : false,
                            child: Text("班级：${widget.userInfo?["deptid"]}", style: _propleInfo,),
                          ),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                          Text(" ", style: _propleInfo,),
                        ],
                      )
                  ),
                ),
              ),

            ],
          )
      ),
    );
  }
}
