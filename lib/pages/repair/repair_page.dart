import 'package:flutter/material.dart';

class RepairPage extends StatefulWidget {
  const RepairPage({super.key});

  @override
  State<RepairPage> createState() => _RepairPageState();
}

class _RepairPageState extends State<RepairPage> {



  @override
  Widget build(BuildContext context) {
    double win_height = MediaQuery.of(context).size.height;
    //print(win_height);
    return Container(
      margin: EdgeInsets.all(5),
      color: Colors.grey.withAlpha(33),
      child: Column(
        children: [
          TitlePanel(),
          getForm(win_height-117),
        ],
      ),
    );

  }

  Widget TitlePanel() {
    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 15),
      child: Text("设备检修", style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold,),),
    );
  }

  Widget getForm(double win_height) {

    return SizedBox(
      height: win_height,
      child: SingleChildScrollView(
        child: Wrap(
          direction: Axis.horizontal,
          runSpacing: 10,
          spacing: 10,
          children: [
            TextFormField(
              autofocus: true,
              decoration: InputDecoration(
                labelText: "教职员/学生",
                hintText: "请输入教职员/学生",
                icon: Icon(Icons.psychology),
              ),
            ),
            TextFormField(),
            TextFormField(),
            TextFormField(),
            Container(
              margin: EdgeInsets.all(5),
              width: 160,
              height: 100,
              color: Colors.grey.withAlpha(33),
              child: Text("QQQ"),
            ),
            Container(
              margin: EdgeInsets.all(5),
              width: 160,
              height: 100,
              color: Colors.grey.withAlpha(33),
              child: Text("QQQ"),
            ),
            Container(
              margin: EdgeInsets.all(5),
              width: 160,
              height: 100,
              color: Colors.grey.withAlpha(33),
              child: Text("QQQ"),
            ),









          ],
        ),


      ),
    );

  }


}
