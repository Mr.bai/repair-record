
import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/common/diglog_page.dart';
import 'package:maintenancerecord/pages/repair/people_info.dart';
import 'package:maintenancerecord/pages/service/repair_service.dart';

class RepairForm extends StatefulWidget {

  final String? accountId;

  final String? userName;

  final String? fullName;

  final String? gender;

  final String? gradelevel;

  final String? deptid;

  final String? cardNo;

  final String? sourceType;

  final String? img;


  const RepairForm({
    super.key,
    this.accountId,
    this.userName,
    this.fullName,
    this.gender,
    this.gradelevel,
    this.deptid,
    this.cardNo,
    this.sourceType,
    this.img
  });


  // "accountId": "karry_bai",
  // "userName": "白珂羊",
  // "fullName": "白珂羊",
  // "gender": "男",
  // "gradelevel": "G-1",
  // "deptid": "5414",
  // "img": "http://intranet.kcisec.com/page/staff/H20010001.jpg",
  // "cardNo": "2699936185",
  // "sourceType": "A"


  @override
  State<RepairForm> createState() => _RepairFormState();
}

class _RepairFormState extends State<RepairForm> {

  String _valu = "我是文字，我会变动。";
  List<int> _selectIndexs = [0,0,0,];
  int selectUserIndex = 0;
  late Map userInfo;

  List<String> _optUser = ["曾聪", "景柯涵", "王聪", "闻清", "吴芳", "白珂羊", ];
  List<String> _optUserEmpNo = ["H19100003", "H19050008", "H23040003", "H20080188", "H23020079", "H20010001", ];
  List<String> _optState = ["已  完  成", "处  理  中", "暂未处理"];
  List<String> _optType = ["硬件", "软件", "网络" , "其他" ];

  final FocusNode _focusNode = FocusNode();
  late TextEditingController _textController;

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController();
  }

  Widget _buildDropdownButton(int i, List<String> items) {

    List<DropdownMenuItem> _buildItems() => items.asMap().keys.map((index) => DropdownMenuItem(
      value: index,
      child: Text(items[index]),
    )).toList();

    return SizedBox(
      width: 120,
      child: DropdownButtonFormField(
        value: _selectIndexs[i],
        elevation: 1,
        hint: const Text("请选择"),
        icon: Icon(
          Icons.expand_more,
          size: 20,
          color: Colors.red,
        ),
        items: _buildItems(),
        onChanged: (v) => setState(() {
          _selectIndexs[i] = v;
        }),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Map args = ModalRoute.of(context)?.settings.arguments as Map;
    // print();
    String empNo = args["empNo"];

    setState(() {
      userInfo = args;
    });
    double win_height = MediaQuery.of(context).size.height;
    double win_w = MediaQuery.of(context).size.width - 280;

    return Scaffold(
      appBar: AppBar(
        title: Text("故障记录"),
      ),
      body: SizedBox(
        // height: win_height,
          child: Row(
            children:  [

              PeopleInfoPage(winHeight: win_height, userInfo: userInfo,),

              Expanded(
                child: SizedBox(
                  child: Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: SingleChildScrollView(
                      child: Wrap(
                        direction: Axis.horizontal,
                        runSpacing: 10,
                        spacing: 10,
                        children: [



                          Container(
                            width: win_w/3,
                            // color: Colors.grey,
                            child: Row(
                              children: [
                                Text("操作员"),
                                SizedBox(width: 10,),
                                _buildDropdownButton(0, _optUser),
                              ],
                            ),
                          ),

                          Container(
                            width: win_w/3,
                            child: Row(
                              children: [
                                Text("故障类型"),
                                SizedBox(width: 10,),
                                _buildDropdownButton(1, _optType),
                              ],
                            ),
                          ),

                          Container(
                            width: win_w/3,
                            child: Row(
                              children: [
                                Text("处理结果"),
                                SizedBox(width: 10,),
                                _buildDropdownButton(2, _optState),
                              ],
                            ),
                          ),

                          Container(
                            width: win_w,
                            height: 450,
                            // color: Colors.grey.withAlpha(33),
                            child: Column(
                              children: [
                                Container(
                                  width: win_w,
                                  child: Text("故障描述"),
                                ),
                                TextField(
                                  controller: _textController,
                                  minLines: 15,
                                  maxLines: 18,
                                  autofocus: true,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    // labelText: "故障内容",
                                    hintText: "请输入故障内容及处理结果,并备注",
                                    // icon: Icon(Icons.psychology),
                                  ),
                                  onEditingComplete: () {
                                    print('onEditingComplete');
                                  },
                                  onChanged: (v) {
                                    print('onChanged:' + v);
                                  },
                                  onSubmitted: (v) {
                                    // FocusScope.of(context).requestFocus(_focusNode);
                                    print('onSubmitted:' + v);
                                    // _controller.clear();
                                  },


                                ),
                              ],
                            ),
                          ),


                          Container(
                            margin: EdgeInsets.all(5),
                            width: 500,
                            height: 100,
                            child: Center(
                              child: ElevatedButton.icon(
                                  onPressed: () => {
                                    submitRecord(userInfo["empNo"], _optUserEmpNo[_selectIndexs[0]], _selectIndexs[1].toString(), _selectIndexs[2].toString(), _textController.text),
                                  },
                                  style: ButtonStyle(
                                    padding: MaterialStateProperty.all(EdgeInsets.all(18)),
                                  ),
                                  icon: Icon(Icons.save, color: Colors.white),
                                  label: Text("保 存", style: TextStyle(color: Colors.white))
                              ),
                            ),
                          ),


                        ],
                      ),


                    ),
                  )

                ),
              ),


            ],
          )),
    );
  }

  submitRecord(String empNo, String optUser, String optType, String optState, String optRemark) {
    // int _optType = optType as int;
    // int _optState = optState as int;
    RepairService.saveRecord(context, empNo, optUser, optType, optState, optRemark, (res) {
      //print(res);
      if(res["code"] == 200) {
        SnackBar sb = SnackBar(
          backgroundColor: Colors.lightGreen,
          content: Text(res["info"]),
        );
        ScaffoldMessenger.of(context).showSnackBar(sb);
        Navigator.pop(context);
      } else {
        DialogTools.showAlertDialog(context, "提示", res["info"]);
      }
    });
  }


}
