import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/common/diglog_page.dart';
import 'package:maintenancerecord/pages/service/user_service.dart';

class RepairSearch extends StatefulWidget {
  const RepairSearch({super.key});

  @override
  State<RepairSearch> createState() => _RepairSearchState();
}

class _RepairSearchState extends State<RepairSearch> {
  int type = 1;

  TextEditingController searchController = TextEditingController();

  _onChangedRadio(value) {
    setState(() {
      this.type = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        // height: 400,
        child: Container(
          // color: Colors.indigo,
          child: Column(
            children: [
              SizedBox(
                width: 600,
                child: Container(
                  height: 165, width: 165,
                ),
              ),
              Expanded(
                  child: Flex(
                    direction: Axis.vertical,
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 600,
                        child: Container(
                          // color: Colors.black12,
                          child: Image.asset("assets/images/favicon.png", height: 165, width: 165,),
                        ),
                      ),
                      SizedBox(
                        width: 600,
                        child: Flex(
                          direction: Axis.horizontal,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text("卡号"),
                            Radio(
                              // 按钮的值
                              value: 1,
                              // 改变事件
                              onChanged: (_) =>_onChangedRadio(_),
                              // 按钮组的值
                              groupValue: this.type,
                            ),

                            SizedBox(width: 20,),
                            Text("学号/工号"),
                            Radio(
                              value:2,
                              onChanged: (_) =>_onChangedRadio(_),
                              groupValue: this.type,
                            ),





                          ],
                        ),
                      ),
                      SizedBox(
                        width: 600,
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                controller: searchController,
                                style: TextStyle(textBaseline: TextBaseline.alphabetic),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: "请输入"
                                ),
                                // validator: _validatorInputText,
                                // onFieldSubmitted: _onFieldSubmitted,
                                // onSaved: _onSaved,

                              ),
                            ),
                            SizedBox(width: 10,),
                            ElevatedButton.icon(
                                onPressed: () => searchUser(),
                                style: ButtonStyle(
                                  padding: MaterialStateProperty.all(EdgeInsets.all(22)),
                                ),
                                icon: Icon(Icons.search, color: Colors.white),
                                label: Text("查询", style: TextStyle(color: Colors.white))
                            ),




                          ],
                        ),
                      )
                    ],
                  ),
              )
            ],
          ),
        ),
      ),
    );



  }

  searchUser() {
    print("查询");
    String cardNo = "", empNo = "";
    if(type == 1) {
      cardNo = searchController.text;
    } else if (type == 2) {
      empNo = searchController.text;
    }

    UserService.getUserInfo(context, cardNo, empNo, callBackUser);
  }

  callBackUser(res) {
    var _res = res as Map;
    if(_res["code"] == 200) {
      Navigator.pushNamed(context, "/repair/form", arguments: _res["data"]);  //
    } else {
      DialogTools.showAlertDialog(context, "提示", "未找到相关人员信息");
    }
  }


}

