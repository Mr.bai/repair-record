import 'package:flutter/material.dart';

class ChildWidget extends StatefulWidget {
  final bool active;
  final int pageNums;
  final Function onUpdateActive;

  ChildWidget({Key? key,required this.active,required this.onUpdateActive, required this.pageNums}) : super(key: key);

  @override
  _ChildWidgetState createState() => _ChildWidgetState();
}

class _ChildWidgetState extends State<ChildWidget> {
  bool? _active;
  int _pageNums = 0;

  @override
  void initState() {
    super.initState();
    _active = widget.active;
    _pageNums = widget.pageNums;
  }

  @override
  void didUpdateWidget(ChildWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.active != widget.active) {
      setState(() {
        _active = widget.active;
        _pageNums = widget.pageNums;
      });
    }
  }

  void _handleTap() {
    _active = !_active!;
    _pageNums = _pageNums++;
    widget.onUpdateActive(_active, _pageNums);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      child: Center(
        child: Container(
          // width: ,
          // color: !_active! ? Colors.blue : Colors.grey,
          child: Column(
            children: [
              Container(
                height: 280,
              ),
              Expanded(
                  child: Column(
                    children: [
                      Image.asset("assets/images/favicon.png", height: 65, width: 65,),
                      SizedBox(height: 10,),
                      Text("关于", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35, fontFamily: 'YouSheBiaoTiHei' ),),
                      SizedBox(height: 10,),
                      Text("当前版本: 0.43 Beta", style: TextStyle(fontSize: 18),),
                      Text(""),

                    ],
                  )
              ),
              Container(
                height: 180,
              ),

            ],
          ),
        ),
      ),
    );
  }
}