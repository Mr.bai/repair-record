import 'package:flutter/material.dart';

class SettingIndexPage extends StatefulWidget {

  final String content;

  const SettingIndexPage({Key? key, required this.content}) : super(key: key);

  @override
  State<SettingIndexPage> createState() => _SettingIndexPageState();
}

class _SettingIndexPageState extends State<SettingIndexPage> {
  @override
  Widget build(BuildContext context) {

    int? _selectV = 3;
    _onChangedSelect (int v) {
      setState(() {
        _selectV = v;
      });
    }

    return Center(
        child: DropdownButton<int>(
          value: _selectV,
          // elevation: 1,
          icon: Icon(
            Icons.expand_more,
            size: 20,
            color: Colors.red,
          ),
          items: [
            DropdownMenuItem(
              value: 1,
              child: Text("QQQ1"),
            ),
            DropdownMenuItem(
              value: 2,
              child: Text("QQQ2"),
            ),
            DropdownMenuItem(
              value: 3,
              child: Text("QQQ3"),
            ),
          ],
          onChanged: (v) {
            _onChangedSelect(v!);
          },
        )
        // Text(
        //   widget.content,
        //   style: const TextStyle(fontSize: 26),
        // )
    );
  }
}
