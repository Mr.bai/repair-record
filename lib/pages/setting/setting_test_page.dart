import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/setting/setting_test_page_2.dart';

class ParentWidget extends StatefulWidget {
  const ParentWidget({super.key});

  @override
  _ParentWidgetState createState() => _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidget> {
  bool _active = false;
  int _pageNums = 996;

  void _handleTap() {
    setState(() {
      _active = !_active;
      _pageNums = _pageNums++;
    });
  }

  void _updateActive(bool value, int nums) {
    print("我回来了${value}, ${nums}");
    setState(() {
      _active = value;
      _pageNums = nums;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      child: Container(
        child: ChildWidget(active: _active, onUpdateActive: _updateActive, pageNums: _pageNums,),
      ),
    );
  }
}