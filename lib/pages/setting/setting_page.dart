import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/setting/home_content_page.dart';
class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      width: 300,
      child: Navigator(
        onPopPage: _onPopPage,
        initialRoute: "/home-content",
        onGenerateRoute: _onGenerateRoute,
        observers: [TolyNavigatorObservers()],
      ),
    );
  }


  /// 这是创建Widget时调用的除构造方法外的第一个方法
  /// 类似于Android的：onCreate() 与 IOS 的 viewDidLoad();
  /// 在这个方法中通常会做一些初始化，比如channel的初始化，监听器的初始化等
  @override
  void initState() {
    // TODO: implement initState
    print('------------initState---------------');
    super.initState();
  }

  /// 当依赖的State对象改变时会调用
  /// a 在第一次构建widget时，在initState()之后立即调用此方法
  /// b 如果StatefulWidget依赖于InheritedWidget，那么当 当前的State所依赖InheritedWidget中的变量改变时会再次调用它
  /// 拓展：InheritedWidget可以高效的将数据在Widget树中向下传递、共享；
  @override
  void didChangeDependencies() {
    print('--------------didChangeDependencies--------------');
    super.didChangeDependencies();
  }


  /// 这是一个不常用到的生命周期方法，当父组件需要重新绘制时才会调用；
  /// 该方法会携带一个oldWidget参数，可以将与当前的Widget进行对比以便执行一些额外的逻辑，如
  /// if(oldWidget.xxx != widget.xxx) {...}
  @override
  void didUpdateWidget(covariant SettingPage oldWidget) {
    print('--------------didUpdateWidget--------------');
    super.didUpdateWidget(oldWidget);
  }

  /// 很少使用，在组件被移除的时候调用dispose之前调用
  @override
  void deactivate() {
    print('-------------deactivate--------------');
    super.deactivate();
  }

  /// 通常，组件被销毁时调用
  /// 通常在方法中执行一些资源的释放工作，比如监听器的卸载、channel的销毁等
  @override
  void dispose() {
    print('------------dispose-------------');
    super.dispose();
  }

  bool _onPopPage(Route<dynamic> route, result) {
    print('-------_onPopPage------');
    return true;
  }

  Route _onGenerateRoute (RouteSettings settings) {
    switch (settings.name) {
      case '/home-content':
        return MaterialPageRoute(builder: (_) => const HomeContent(), settings: settings);
      case "/red":
        return MaterialPageRoute(builder: (_) => const RedPage(), settings: settings);
      case "/yellow":
        return MaterialPageRoute(builder: (_) => const YellowPage(), settings: settings);
      case "/green":
        return MaterialPageRoute(builder: (_) => const GreenPage(), settings: settings);
      default:
        return MaterialPageRoute(builder: (_) => const HomeContent(), settings: settings);
    }
  }

}

//路由监听器
class TolyNavigatorObservers extends NavigatorObserver {
  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    print(
        '--didPush:--route:--${route.settings}--previousRoute:--${previousRoute?.settings}');
  }

  @override
  void didStopUserGesture() {
    print('--didStopUserGesture:--');
  }

  @override
  void didStartUserGesture(Route<dynamic> route, Route<dynamic>? previousRoute) {
    print(
        '--didStartUserGesture:--route:--${route.settings}--previousRoute:--${previousRoute?.settings}');
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    print(
        '--didReplace:--newRoute:--${newRoute?.settings}--oldRoute:--${oldRoute?.settings}');
  }

  @override
  void didRemove(Route<dynamic>? route, Route<dynamic>? previousRoute) {
    print(
        '--didRemove:--route:--${route?.settings}--previousRoute:--${previousRoute?.settings}');
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    print(
        '--didPop:--route:--${route.settings}--previousRoute:--${previousRoute?.settings}');
  }
}












