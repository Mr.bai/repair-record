import 'package:flutter/material.dart';

class HomeContent extends StatelessWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            ElevatedButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                shape: const CircleBorder(
                  side: BorderSide(width: 2.0, color: Color(0xFFDFDFDF)),
                ),
              ),
              child: const Text('to red'),
              onPressed: () {
                Navigator.pushNamed(context, '/red');
              },
            ),
            ElevatedButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.yellow,
                shape: const CircleBorder(
                  side: BorderSide(width: 2.0, color: Color(0xFFDFDFDF)),
                ),
              ),
              child: const Text('to yellow'),
              onPressed: () {
                Navigator.pushNamed(context, '/yellow');
              },
            ),
            ElevatedButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.green,
                shape: const CircleBorder(
                  side: BorderSide(width: 2.0, color: Color(0xFFDFDFDF)),
                ),
              ),
              child: const Text('to yellow'),
              onPressed: () {
                Navigator.pushNamed(context, '/green');
              },
            )
          ],
        ),
      ],
    );
  }
}

class RedPage extends StatelessWidget {
  const RedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("RedPage"),
      ),
      body: Container(
        color: Colors.red,
      ),
    );
  }
}

class YellowPage extends StatefulWidget {
  const YellowPage({super.key});

  @override
  State<YellowPage> createState() => _YellowPageState();
}

class _YellowPageState extends State<YellowPage> {

  String _valu = "我是文字，我会变动。";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("YellowPage"),
      ),
      body: Container(
        color: Colors.yellow,
        child: Column(
          children: [
            Text(_valu),
            ElevatedButton(onPressed: (){
              setState(() {
                _valu = "看吧，我就说会变吧";
              });
            }, child: Text("点我就会发生改变"))
          ],
        ),
      ),
    );
  }
}



class GreenPage extends StatelessWidget {
  const GreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("GreenPage"),
      ),
      body: Container(
        color: Colors.green,
      ),
    );
  }
}