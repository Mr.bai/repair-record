import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/common/topbar_page.dart';

class HeroInfo {
  HeroInfo(this.name, this.calories, this.fat, this.carbs);

  final String name;
  final String calories;
  final String fat;
  final String carbs;
  bool selected = false;
}

class DessertDataSource extends DataTableSource {
  final List<HeroInfo> _desserts = [
    HeroInfo('捷特', '《幻将录》', "人族", "男"),
    HeroInfo('龙少', '《幻将录》', "人族", "男"),
    HeroInfo('巫缨', '《幻将录》', "人族", "女"),
    HeroInfo('林兮', '《幻将录》', "人族", "男"),
    HeroInfo('九方玄玉', '《风神传》', "神族", "男"),
    HeroInfo('七日洪荒', '《风神传》', "魔族", "男"),
    HeroInfo('林昔瑶', '《封妖志》', "鬼族", "女"),
    HeroInfo('林兮鬼帝', '《封妖志》', "鬼族", "男"),
    HeroInfo('艾隆', '《封妖志》', "鬼族", "男"),
    HeroInfo('语熙华', '《风神传》', "道族", "男"),
    HeroInfo('雪玉宛如', '《幻将录》', "人族", "女"),
    HeroInfo('破千', '《幻将录》', "人族", "男"),
    // HeroInfo('浪封', '《幻将录》', "人族", "男"),
    // HeroInfo('虎翼穷奇', '《封妖志》', "妖族", "男"),
    // HeroInfo('凯', '《幻将录》', "人族", "男"),
    // HeroInfo('荆棘', '《幻将录》', "人族", "女"),
    // HeroInfo('龙右', '《幻将录》', "人族", "男"),
    // HeroInfo('梦千', '《幻将录》', "人族", "男"),
    // HeroInfo('梦小梦', '《幻将录》', "人族", "女"),
    // HeroInfo('梦瞳', '《幻将录》', "人族", "男"),
    // HeroInfo('十戈', '《幻将录》', "人族", "男"),
    // HeroInfo('计画天', '《幻将录》', "人族", "女"),
    // HeroInfo('士方', '《幻将录》', "人族", "男"),
    // HeroInfo('巫妻孋', '《幻将录》', "人族", "女"),
    // HeroInfo('木时黎', '《永恒传说》', "人族", "男"),
    // HeroInfo('木艾奇', '《永恒传说》', "人族", "男"),
    // HeroInfo('张风', '《永恒传说》', "人族", "男"),
    // HeroInfo('薛剑儿', '《永恒传说》', "人族", "男"),
    // HeroInfo('李月', '《永恒传说》', "人族", "女"),
    // HeroInfo('刘雪', '《永恒传说》', "人族", "女"),
    // HeroInfo('葛心', '《永恒传说》', "人族", "女"),
    // HeroInfo('步映容', '《幻将录》', "人族", "女"),
    // HeroInfo('莫慈良', '《幻将录》', "人族", "男"),
    // HeroInfo('莫向阳', '《幻将录》', "人族", "男"),
    // HeroInfo('莫子薇', '《永恒传说》', "人族", "女"),
    // HeroInfo('藏凯阳', '《永恒传说》', "人族", "男"),
    // HeroInfo('奇雨歆', '《永恒传说》', "人族", "女"),
    // HeroInfo('林天蕊', '《永恒传说》', "人族", "女"),
    // HeroInfo('吴灏然', '《永恒传说》', "人族", "男"),
    // HeroInfo('何解连', '《永恒传说》', "人族", "男"),
    // HeroInfo('步络尘', '《幻将录》', "人族", "男"),
    // HeroInfo('拓雷', '《幻将录》', "人族", "男"),
    // HeroInfo('炽阳骑', '《幻将录》', "人族", "男"),
    // HeroInfo('正构', '《幻将录》', "人族", "男"),
    // HeroInfo('烈', '《幻将录》', "人族", "男"),
    // HeroInfo('梦华君', '《幻将录》', "人族", "男"),
    // HeroInfo('初星', '《幻将录》', "人族", "男"),
    // HeroInfo('梦飞烟', '《幻将录》', "人族", "男"),
    // HeroInfo('武落英', '《幻将录》', "人族", "女"),
    // HeroInfo('古千缘', '《幻将录》', "人族", "男"),
    // HeroInfo('古千缘', '《幻将录》', "人族", "男"),
  ];

  int _selectedCount = 0;

  @override
  DataRow? getRow(int index) {
    if (index >= _desserts.length) return null;
    final HeroInfo dessert = _desserts[index];
    return DataRow.byIndex(
        index: index,
        selected: dessert.selected,
        // onSelectChanged: (bool? value) {
        //   if (dessert.selected != value && value != null) {
        //     _selectedCount += value ? 1 : -1;
        //     assert(_selectedCount >= 0);
        //     dessert.selected = value;
        //     notifyListeners();
        //   }
        // },
        cells: <DataCell>[
          DataCell(Text(dessert.name)),
          DataCell(Text(dessert.calories)),
          DataCell(Text(dessert.fat)),
          DataCell(Text(dessert.carbs)),
        ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _desserts.length;

  @override
  int get selectedRowCount => _selectedCount;

  void selectAll(bool? checked) {
    if (checked == null) return;
    for (HeroInfo dessert in _desserts) {
      dessert.selected = checked;
    }
    _selectedCount = checked ? _desserts.length : 0;
    notifyListeners();
  }


}
