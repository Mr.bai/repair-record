import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/common/diglog_page.dart';
import 'package:maintenancerecord/pages/service/repair_service.dart';

class RecordComment extends StatefulWidget {

  final Map info;

  const RecordComment({super.key, required this.info});

  @override
  State<RecordComment> createState() => _RecordCommentState();
}

class _RecordCommentState extends State<RecordComment> {

  TextEditingController _textController=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: _buildTitle(),
      titleTextStyle: const TextStyle(fontSize: 20, color: Colors.black),
      titlePadding: const EdgeInsets.only(
        top: 5,
        left: 20,
      ),
      contentPadding: const EdgeInsets.symmetric(horizontal: 5),
      backgroundColor: Colors.white,
      content: _buildContent(widget.info),
      actions:  [
        ElevatedButton.icon(
            onPressed: () => {
              // print(_selectIndexs),
              // print(widget.info["id"]),
              // print(_optUserEmpNo[_selectIndexs[0]]),
              // print(_optUser[_selectIndexs[0]]),
              //
              // print(_optState[_selectIndexs[1]]),
              //
              // print(_textController.text),
              save(widget.info["recordId"].toString(), _optUserEmpNo[_selectIndexs[0]], _selectIndexs[1].toString(), _textController.text.toString()),
            },
            style: ButtonStyle(
              padding: MaterialStateProperty.all(EdgeInsets.all(18)),
            ),
            icon: Icon(Icons.save, color: Colors.white),
            label: Text("保 存", style: TextStyle(color: Colors.white))
        ),
      ],
      elevation: 4,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
    );


  }


  save(String recordId,String user, String state, String desc) {
    RepairService.saveComment(context, recordId, user, state, desc).then((response)  {
      //print(response.statusCode);

      if(response.statusCode == 200) {
        Map res = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
        if(res["code"] == 200) {
          SnackBar sb = SnackBar(
            backgroundColor: Colors.lightGreen,
            content: Text(res["info"]),
          );
          ScaffoldMessenger.of(context).showSnackBar(sb);
          Navigator.pop(context);
        } else {
          DialogTools.showAlertDialog(context, "提示", res["info"]);
        }
      } else {
        DialogTools.showAlertDialog(context, "提示", "系统异常：500");
      }
    });
  }


  Widget _buildTitle() {
    return Row(
      //标题
      children: <Widget>[
        // Image.asset(
        //   "assets/images/icon_head.webp",
        //   width: 30,
        //   height: 30,
        // ),
        const SizedBox(width: 10,),
        const Expanded(
            child:  Text(
              "继续维修",
              style: TextStyle(fontSize: 18),
            )),
        const CloseButton()
      ],
    );
  }

  Widget _buildContent(Map info) {
    return SizedBox(
      width: 560,
      child: Container(
        // color: Colors.black45,
          margin: EdgeInsets.only(top: 10, bottom: 10),
          child: SingleChildScrollView(
            child: Wrap(
              direction: Axis.horizontal,
              runSpacing: 10,
              spacing: 10,
              children: [
                Container(
                  width: (560-10)/2,
                  child: Row(
                    children: [
                      Text("保修人员"),
                      SizedBox(width: 10,),
                      Text(info["reportUserName"]),
                    ],
                  ),
                ),
                Container(
                  width: (560-10)/2,
                  child: Row(
                    children: [
                      Text("处理结果"),
                      SizedBox(width: 10,),
                      Text(_optType[info["repairType"]]),
                    ],
                  ),
                ),

                Container(
                  width: (560-10)/2,
                  // color: Colors.grey,
                  // child: Text("Q"),
                  child: Row(
                    children: [
                      Text("操作员"),
                      SizedBox(width: 10,),
                      _buildDropdownButton(0, _optUser),
                    ],
                  ),
                ),

                Container(
                  width: (560-10)/2,
                  // color: Colors.lightGreen,
                  // child: Text("2Q"),
                  child: Row(
                    children: [
                      Text("处理结果"),
                      SizedBox(width: 10,),
                      _buildDropdownButton(1, _optState),
                    ],
                  ),
                ),



                Container(
                  width: 560-10,
                  height: 230,
                  child: Column(
                    children: [
                      Container(
                        width: 560-10,
                        child: Text("故障描述"),
                      ),
                      TextField(
                        controller: _textController,
                        minLines: 8,
                        maxLines: 8,
                        autofocus: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          // labelText: "故障内容",
                          hintText: "请输入故障内容及处理结果,并备注",
                          // icon: Icon(Icons.psychology),
                        ),
                      ),
                    ],
                  ),
                ),





              ],
            ),
          )
      ),
    );

  }


  List<int> _selectIndexs = [0,0,];
  int selectUserIndex = 0;
  late Map userInfo;

  List<String> _optUser = ["曾聪", "景柯涵", "王聪", "闻清", "吴芳", "白珂羊", ];
  List<String> _optUserEmpNo = ["H19100003", "H19050008", "H23040003", "H20080188", "H23020079", "H20010001", ];
  List<String> _optState = ["已  完  成", "处  理  中", "暂未处理"];
  List<String> _optType = ["硬件", "软件", "网络" , "其他" ];


  Widget _buildDropdownButton(int i, List<String> items) {

    List<DropdownMenuItem> _buildItems() => items.asMap().keys.map((index) => DropdownMenuItem(
      value: index,
      child: Text(items[index]),
    )).toList();

    return SizedBox(
      width: 120,
      child: DropdownButtonFormField(
        value: _selectIndexs[i],
        elevation: 1,
        hint: const Text("请选择"),
        icon: Icon(
          Icons.expand_more,
          size: 20,
          color: Colors.red,
        ),
        items: _buildItems(),
        onChanged: (v) => setState(() {
          _selectIndexs[i] = v;
        }),
      ),
    );
  }

}
