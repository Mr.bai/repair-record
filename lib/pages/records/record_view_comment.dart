import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:maintenancerecord/pages/common/common_attr.dart';
import 'package:maintenancerecord/pages/common/diglog_page.dart';
import 'package:maintenancerecord/pages/service/repair_service.dart';

class RecordViewComment extends StatefulWidget {

  final int recordId;

  const RecordViewComment({super.key, required this.recordId});

  @override
  State<RecordViewComment> createState() => _RecordViewCommentState();
}

class _RecordViewCommentState extends State<RecordViewComment> {

  int _postion = -1;
  List plist = [];

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadComments(widget.recordId);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: _buildTitle(),
      titleTextStyle: const TextStyle(fontSize: 20, color: Colors.black),
      titlePadding: const EdgeInsets.only(
        top: 5,
        left: 20,
      ),
      contentPadding: const EdgeInsets.symmetric(horizontal: 5),
      backgroundColor: Colors.white,
      content: _buildContent(),
      actions:  [],
      elevation: 4,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
    );
  }



  Widget _buildTitle() {
    return Row(
      //标题
      children: <Widget>[
        const SizedBox(width: 10,),
        const Expanded(
            child:  Text(
              "历史记录",
              style: TextStyle(fontSize: 18),
            )),
        const CloseButton()
      ],
    );
  }




  Widget _buildContent() {


    return SizedBox(
      width: 560,
      height: 300,
      child: Container(
        // margin: EdgeInsets.only(top: 10, bottom: 10),
        child: SingleChildScrollView(
          child: ExpansionPanelList(
            expandIconColor: Colors.black87,
            dividerColor: Colors.black87,
            children: convertPanelData(plist),
            expandedHeaderPadding: EdgeInsets.zero,
            animationDuration: const Duration(milliseconds: 200),
            expansionCallback: (index, open) {
              setState(() => (
                  _postion = (open) ? -1 : index,
              ));
            },
          ),
        ),
      ),
    );







  }

  loadComments(int recordId) {
    Future _response = RepairService.getComments(context, recordId.toString());
    _response.then((response) {
      if(response.statusCode == 200) {
        Map res = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
        if(res["code"] == 200) {
          List list = res["data"];
          setState(() {
            plist = list;
          });
        } else {
          DialogTools.showAlertDialog(context, "提示", res["info"]);
        }
      }
    });
  }



  List<ExpansionPanel> convertPanelData(List listData) {
    List<ExpansionPanel> list = [];
    for(int i = 0;i < listData.length; i++) {

      Map _dm = listData[i];
      list.add(ExpansionPanel(
        backgroundColor: Color(0XFFF5F2F0),
        isExpanded: i == _postion,
        headerBuilder: (ctx, index) => Container(

          child: Row(
            children: <Widget>[
              Container(
                width: 150,
                height: 20,
                alignment: Alignment.center,
                child: Text(
                  _dm["commentUserName"],
                  style: const TextStyle(color: Colors.black),
                ),
              ),
              Container(
                width: 80,
                height: 20,
                alignment: Alignment.center,
                child: Text(
                  CommonAttr.optState[_dm["commentState"]],
                  style: const TextStyle(color: Colors.black),
                ),
              ),

              Expanded(
                child: Container(
                  height: 20,
                  alignment: Alignment.centerRight,
                  child: Text(
                    _dm["createTime"],
                    style: const TextStyle(color: Colors.black),
                  ),
                ),
            ),

            ],
          ),
        ),
        body: Container(
          alignment: Alignment.topLeft,
          height: 120,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              top: BorderSide(width: 0.1, color: Colors.black),
              left: BorderSide(width: 0.1, color: Colors.black),
              right: BorderSide(width: 0.1, color: Colors.black),
              bottom: BorderSide(width: 0.1, color: Colors.black),
            ),
          ),
          child: SingleChildScrollView(
            padding:  EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            child: Container(
              width: 560,
              child: Text(
                _dm["commentDesc"],
                style: const TextStyle(color: Colors.black, shadows: [
                  Shadow(
                    color: Colors.black,
                    offset: Offset(.5, .5),
                    blurRadius: 2,
                  )
                ]),
              ),
            ),
          ),




        ),
      ));

    }

    return list;
  }


}


