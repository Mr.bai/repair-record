// Don't forget to make the changes mentioned in
// https://github.com/bitsdojo/bitsdojo_window#getting-started

import 'package:flutter/material.dart';
import 'package:bitsdojo_window/bitsdojo_window.dart';

import 'package:maintenancerecord/pages/common/index_page.dart';
import 'package:maintenancerecord/pages/repair/repair_form.dart';
import 'package:maintenancerecord/pages/repair/repair_search.dart';

void main() {
  //必须配置
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const RunApp());

  doWhenWindowReady(() {
    final win = appWindow;
    const initialSize = Size(1024, 768);
    win.minSize = initialSize;
    // win.maxSize = initialSize;
    win.size = initialSize;
    win.alignment = Alignment.center;
    win.title = "维修检测工具";
    win.show();
  });

}

class RunApp extends StatefulWidget {
  const RunApp({super.key});

  @override
  State<RunApp> createState() => _RunAppState();
}

class _RunAppState extends State<RunApp> {


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // debugShowMaterialGrid: true,
      // showPerformanceOverlay: true,
      home :  IndexPage(),
      // routes: _routes,
    );
  }

  @override
  void dispose() {

  }


}
