# RepairRecord（维修检测工具App）

一个基于Flutter开发运行在多平台( Windows | Mac | Liunx )的维修检测工具。
用于记录企业及校园内部人员的维修保修过程，以便于后期追溯。

---

> 支持平台

| Windows 10                         | Mac                          | Liunx                         |
|---------------------------|---------------------------|---------------------------|
| 支持(Windows 10及以上)| 支持(MacOS 13及以上) | 支持(Deepin 等) |

---

> 当前Flutter 版本

```
Flutter 3.10.0 • channel stable • https://github.com/flutter/flutter.git
Framework • revision 84a1e904f4 (4 weeks ago) • 2023-05-09 07:41:44 -0700
Engine • revision d44b5a94c9
Tools • Dart 3.0.0 • DevTools 2.23.1
```

---

#### 维修检测工具App 界面()

> Windows 截图

| .                         | .                         | .                         |
|---------------------------|---------------------------|---------------------------|
| ![](./screenshot/001.png) | ![](./screenshot/002.png) | ![](./screenshot/003.png) |
| ![](./screenshot/004.png) | ![](./screenshot/005.png) | ![](./screenshot/006.png) |
| ![](./screenshot/007.png)  |  |  |

---

> Mac OS 截图

暂无

---

> Linux 截图

暂无

---

#### 更新日志：


> ### 2023-09-01(v0.43 Beta)
> 1. 修复数据列表时间控件无法清空的问题
> 2. 修复数据列表无法查询的问题

---
